
var productosObtenidos;
function getProductos (){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200){
        console.table(JSON.parse(request.responseText).value);
        productosObtenidos = request.responseText;
        procesarProds();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProds (){
  var JSONProducts = JSON.parse(productosObtenidos);
  //alert(JSONProducts.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.getElementById("table");
  var tbody = document.getElementById("tbody");
  tabla.classlist.add("table");
  tabla.classlist.add("table-striped");
  for (var i = 0; i < JSONProducts.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProducts.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaNombre.innerText = JSONProducts.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaNombre.innerText = JSONProducts.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
//( ){ }[ ]
